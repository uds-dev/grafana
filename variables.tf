variable "acm_certificate" {
  description = "ACM Certificate for TLS termination in ALB"
  type        = string
}

variable "aws_ecs_cluster_id" {
  description = "Deploy grafana in the provided ECS cluster"
  type        = string
}

variable "app_container_cpu" {
  description = "cpu usage of app's container"
  type        = number
  default     = 512
}

variable "app_container_memory" {
  description = "memory usage of app's container"
  type        = number
  default     = 1024
}

variable "aws_region" {
  description = "region to deploy in"
  type        = string
}

variable "grafana_desired_count_app" {
  description = "How many tasks? Each task is grafana"
  type        = number
  default     = 1
}

variable "grafana_docker_image" {
  description = "Docker image you want to use as the grafana"
  type        = string
  default     = "grafana/grafana:7.3.7"
}

variable "grafana_plugins" {
  description = "List of plugins to install"
  type        = string
  default     = "raintank-worldping-app"
}

variable "task_cpu" {
  description = "cpu usage of ecs task"
  type        = number
  default     = 1024
}

variable "task_memory" {
  description = "memory usage of ecs task"
  type        = number
  default     = 2048
}

variable "vpc_id" {
  description = "VPC ID that this app will be deployed to"
  type        = string
}

variable "vpc_cidr_ab" {
  description = "VPC CIDR AB, examples: 10.0"
  type        = string
  default     = "172.1"
}

variable "vpc_subnets_private_ids" {
  description = "Private Subnets list of ECS tasks - these tasks can receive traffic only from ALB"
  type        = list(string)
}

variable "vpc_subnets_public_ids" {
  description = "Public Subnets for ALB"
  type        = list(string)
}

variable "efs_id" {
  description = "EFS ID for grafana storage"
  type        = string
}

#variable "grafana_google_client_secret" {
#  description = "grafana google secret."
#}