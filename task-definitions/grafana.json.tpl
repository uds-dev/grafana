[
  {
    "name": "grafana",
    "image": "${app_image}",
    "cpu": ${app_container_cpu},
    "memory": ${app_container_memory},
    "portMappings": [{
      "containerPort": 3000,
      "protocol": "tcp"
    }],
    "environment": [
      { "name": "GF_INSTALL_PLUGINS", "value": "${grafana_plugins}" }
    ],
    "mountPoints": [{
      "containerPath": "/var/lib/grafana",
      "sourceVolume": "efs-grafana-storage"
    }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "grafana",
        "awslogs-group": "${log_group_app}",
        "awslogs-region": "${log_group_region}"
      }
    }
  }
]
