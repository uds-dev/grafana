data "template_file" "grafana_task_definition" {
  template = file("${path.module}/task-definitions/grafana.json.tpl")

  vars = {
    log_group_region       = var.aws_region
    log_group_app          = aws_cloudwatch_log_group.grafana.name
    app_container_cpu      = var.app_container_cpu
    app_container_memory   = var.app_container_memory
    app_image              = var.grafana_docker_image
    grafana_plugins        = var.grafana_plugins
  }
}

resource "aws_ecs_task_definition" "grafana" {
  family                = "grafana"
  container_definitions = data.template_file.grafana_task_definition.rendered
  network_mode          = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  volume {
    name = "efs-grafana-storage"

    efs_volume_configuration {
      file_system_id = var.efs_id
      root_directory = "/grafana-storage"
    }
  }
  
  execution_role_arn = aws_iam_role.ecs_task.arn
  task_role_arn      = aws_iam_role.ecs_task.arn

  cpu    = var.task_cpu
  memory = var.task_memory

}

resource "aws_ecs_service" "grafana" {
  name            = "grafana"
  platform_version = "1.4.0"
  cluster         = var.aws_ecs_cluster_id
  desired_count   = var.grafana_desired_count_app
  task_definition = aws_ecs_task_definition.grafana.arn
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_alb_target_group.grafana.id
    container_name   = "grafana"
    container_port   = "3000"
  }

  network_configuration {
    security_groups = ["${aws_security_group.grafana.id}"]
    subnets         = var.vpc_subnets_private_ids
  }

  depends_on = [
    aws_alb_listener.front_end_80,
    aws_alb_listener.front_end_443,
  ]

}
