resource "aws_alb_target_group" "grafana" {
  name        = "grafana"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip" # with networkmode awsvpc

  health_check {
    path                = "/api/health"
    matcher             = "200"
    timeout             = "5"
    healthy_threshold   = "5"
    unhealthy_threshold = "10"
  }
}

resource "aws_alb" "front" {
  name            = "grafana-front-alb"
  internal        = true
  security_groups = ["${aws_security_group.web.id}"]
  subnets         = var.vpc_subnets_private_ids

  enable_deletion_protection = false

  tags = {
    Name = "grafana"
  }
}

resource "aws_alb_listener" "front_end_80" {
  load_balancer_arn = aws_alb.front.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "front_end_443" {
  load_balancer_arn = aws_alb.front.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.acm_certificate
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    target_group_arn = aws_alb_target_group.grafana.arn
    type             = "forward"
  }
}
